package com.techu.apitechuv2.services;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    /*injección de dependencia (DI - dependece Injection) te permite hacer FINDBYID(REPO,ID)*/
    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){

        return this.productRepository.findAll();
    }
    public ProductModel add(ProductModel product){
        System.out.print("add");
        return  this.productRepository.save(product);
    }
    public Optional<ProductModel> findById(String id){
        System.out.print("findById");
        System.out.print("Obteniendo el producto con la id" + id);
        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel product){
        System.out.print("updateById");
        return  this.productRepository.save(product);
    }

    //public Optional<ProductModel> deleteById(String id){
    /*public void deleteById(String id){
        System.out.print("deleteById");
        System.out.print("Borrado del producto con la id" + id);
        //Optional<ProductModel>  result =  this.productRepository.findById(id);
        this.productRepository.deleteById(id);
        //return result;
    }*/
    //otra manera
    public boolean delete(String id) {
        System.out.print("deleteById");
        System.out.print("Borrado del producto con la id" + id);
        boolean result = false;
        if (this.productRepository.findById(id).isPresent()) {
            //sout
            System.out.println("delete");
            this.productRepository.deleteById(id);
            result = true;

        }
        return result;
    }

    }

