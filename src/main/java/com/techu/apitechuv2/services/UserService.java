package com.techu.apitechuv2.services;

import org.apache.catalina.User;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;


    public List<UserModel> getUsers(String sort){
        List<UserModel> result;/* = new ArrayList<>();*/
        System.out.println("findAllSort");
        System.out.println("sort " + sort);
        if (sort != null) {
            result = this.userRepository.findAll(Sort.by("age"));
        }
        else {
            result = this.userRepository.findAll();
        }
        /*if (sort != null){
            if(sort.equals("ASC")) {
                System.out.println("Consulta " + sort);
                result = this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));

            }
            if(sort.equals("DESC")) {
                result = this.userRepository.findAll(Sort.by(Sort.Direction.DESC, "age"));
            }
        }
        else{
            result = this.userRepository.findAll();
        }*/
        return result;
    }
    public UserModel add(UserModel user){
        System.out.print("add");
        return  this.userRepository.save(user);
    }
    public Optional<UserModel> findById(String id){
        System.out.print("findById");
        return  this.userRepository.findById(id);
    }

    public UserModel update(UserModel user){
        System.out.print("updateById");
        return  this.userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.print("deleteById");
        System.out.print("Borrado del usuario con la id" + id);
        boolean result = false;
        if (this.userRepository.findById(id).isPresent()) {
            //sout
            System.out.println("delete");
            this.userRepository.deleteById(id);
            result = true;

        }
        return result;
    }



}
