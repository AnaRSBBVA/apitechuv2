package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.UserModel;
import com.techu.apitechuv2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins="*", methods ={RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
public class UserController {

    @Autowired
    UserService userService;
    @GetMapping("/users")
    //  http://localhost:8081/apitechu/v2/users/?$orderby=age
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderby", required = false) String orderBy
    ){
        System.out.println("getUsers");
        return new ResponseEntity<>(
                this.userService.getUsers(orderBy), HttpStatus.OK
        );


    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("add user");
        System.out.println("La id del usuario se va a crear es " + user.getId());
        System.out.println("El nombre el usuario es " + user.getName());
        System.out.println("La edad del usuario es " + user.getAge());
        return new ResponseEntity<>(this.userService.add(user), HttpStatus.CREATED);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id ){
        System.out.println("getUser by Id");
        System.out.println("La id del usuario a consultar es " + id );
        Optional<UserModel> result = this.userService.findById(id);
        return  new ResponseEntity<Object>(
                result.isPresent()?result.get():"Usuario no encontrado",
                result.isPresent()?HttpStatus.OK:HttpStatus.NOT_FOUND
        );
    }
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser (@RequestBody  UserModel user, @PathVariable String id ){
        System.out.println("updateUser");
        System.out.println("El nombre del usuario actualizar es  " + user.getName());
        System.out.println("La edad del usuario a actualizar es " + user.getAge());
        user.setId(id);
        Optional<UserModel> result = this.userService.findById(id);
        if(result.isPresent())
            this.userService.update(user);
        return new ResponseEntity<>(user, result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id ) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a eliminar " + id);
        boolean deleteUser = this.userService.delete(id);
        return new ResponseEntity<>(
                deleteUser?"Usuario Borrado" : "Usuario no encontrado",
                deleteUser? HttpStatus.OK: HttpStatus.NOT_FOUND
        );
    }
}
