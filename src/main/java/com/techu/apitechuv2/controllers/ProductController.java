package com.techu.apitechuv2.controllers;

import com.techu.apitechuv2.models.ProductModel;
import com.techu.apitechuv2.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins="*", methods ={RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,RequestMethod.PUT})
public class ProductController {
    @Autowired
    ProductService productService;
    @GetMapping("/products")
    public List<ProductModel> getProducts(){
        System.out.println("getProducts");
        return this.productService.findAll();

    }
    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProducts(@RequestBody  ProductModel product){
        System.out.println("addProducts");
        System.out.println("La id del producto que se va a crear es " + product.getId());
        System.out.println("La descripción del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());
        return new ResponseEntity<>(this.productService.add(product), HttpStatus.CREATED);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id ){
        System.out.println("getProduct by Id");
        System.out.println("La id del producto a consultar es " + id );
        Optional<ProductModel> result = this.productService.findById(id);
       /* if(result.isPresent()) {
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        }
        else {
            /*devolvemos un Object para que no pete el String*/
           /* return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }*/
        return  new ResponseEntity<>(
                result.isPresent()?result.get():"Produco no encontrado",
                result.isPresent()?HttpStatus.OK:HttpStatus.NOT_FOUND
        );
    }
    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct (@RequestBody  ProductModel product, @PathVariable String id ){
        System.out.println("updateProduct");
        System.out.println("La descripción del producto a actualizar " + product.getDesc());
        System.out.println("El precio del producto que se va a actualizar es " + product.getPrice());
        product.setId(id);
        Optional<ProductModel> result = this.productService.findById(id);
        /*if(result.isPresent()) {
            return new ResponseEntity<>(this.productService.add(product), HttpStatus.OK);
        }
        else {
            //devolvemos un Object para que no pete el String
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }*/

        // otra manera de hacerlo
        if(result.isPresent())
            this.productService.update(product);
        return new ResponseEntity<>(product, result.isPresent()? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    /*@DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable String id ) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a eliminar " + id);
        Optional<ProductModel> result = this.productService.findById(id);
        if (result.isPresent()) {
            this.productService.deleteById(id);
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
                    //this.productService.deleteById(id), HttpStatus.OK);
        } else {
            //devolvemos un Object para que no pete el String
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
    }*/

    //OTRA MANERA
    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id ) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a eliminar " + id);
        boolean deleteProduct = this.productService.delete(id);
        return new ResponseEntity<>(
                deleteProduct?"Producto Borrado" : "Producto no encontrado",
                deleteProduct? HttpStatus.OK: HttpStatus.NOT_FOUND
        );

    }

}
