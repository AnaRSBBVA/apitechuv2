package com.techu.apitechuv2.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document (collection = "purchases")
public class PurchaseModel {
   @Id
    private String id;
    private String UserId;
    private Float Amount;
    private Map<String, Integer> purchaseItems;

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userId, Float amount, Map<String, Integer> purchaseItems) {
        this.id = id;
        UserId = userId;
        Amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public Float getAmount() {
        return Amount;
    }

    public void setAmount(Float amount) {
        Amount = amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return purchaseItems;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
